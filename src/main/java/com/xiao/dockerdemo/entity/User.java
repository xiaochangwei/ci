package com.xiao.dockerdemo.entity;

import java.io.Serializable;

/**
 * @Title: com.xiao.dockerdemo.entity.User.java
 * @Description:
 * @author changw.xiao@qq.com
 * @date 2018年1月8日 上午9:18:14
 * @version V1.0
 */
public class User implements Serializable {

	private static final long serialVersionUID = 5872796577493255389L;

	private String userName = "肖哥哥";

	private String email = "changw.xiao@qq.com";

	private String blog = "http://www.cnblogs.com/xiaochangwei/";

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBlog() {
		return blog;
	}

	public void setBlog(String blog) {
		this.blog = blog;
	}

}
