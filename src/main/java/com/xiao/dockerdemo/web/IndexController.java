package com.xiao.dockerdemo.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xiao.dockerdemo.entity.User;

/**
 * @Title: com.xiao.dockerdemo.web.IndexController.java
 * @Description:
 * @author changw.xiao@qq.com
 * @date 2018年1月8日 上午9:17:31
 * @version V1.0
 */
@RestController
public class IndexController {

	@RequestMapping("/profile")
	public User profile() {
		return new User();
	}
}
